$(document).ready(function () {
    var closeModal = $(".close");
    var cancelButton = $(".cancel");
    $("td[data-id='changeCity']").on('click', function () {
        var alias = $(this).data().alias;
        var name = $(this).data().name;
        $("input[name='oldAlias']").val(alias);
        $("input[name='editCityName']").val(name);
        $("input[name='editCityAlias']").val(alias);
        var modal = document.getElementById("editCity");
        modal.style.display = "block";
        closeModal.on('click',function () {
            modal.style.display = "none";
        });
        cancelButton.on('click',function (e) {
            e.preventDefault();
            modal.style.display = "none";
        });
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        };
    });
    $("td[data-id='pickCity']").on('click', function () {
        var alias = $(this).data().alias;
        window.location='/city/viewCity/'+ alias;
    });
    $("td[data-id='deleteCity']").on('click', function () {
        var alias = $(this).data().alias;
        var name = $(this).data().name;
        var modal = document.getElementById("deleteCity");
        $("input[name='deleteCityAlias']").val(alias);
        $("span.confirm-delete").html('Вы уверены, что хотите удалить ' + name + '?');
        modal.style.display = "block";
        closeModal.on('click',function () {
            modal.style.display = "none";
        });
        cancelButton.on('click',function (e) {
            e.preventDefault();
            modal.style.display = "none";
        });
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        };
    });
    $("#addСityButton").on('click', function () {
        var modal = document.getElementById("addCity");
        modal.style.display = "block";
        closeModal.on('click',function () {
            modal.style.display = "none";
        });
        cancelButton.on('click',function (e) {
            e.preventDefault();
            modal.style.display = "none";
        });
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        };
    });
    $("#toList").on('click',function (e) {
        e.preventDefault();
        window.location='/';
    });
    $("#toAvailable").on('click',function (e) {
        e.preventDefault();
        window.location='/city/available';
    });
});
