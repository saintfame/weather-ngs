<?php
namespace weather\controllers;

use weather\core\Controller;
use weather\models\City;
use weather\models\Archive;
class archiveController extends Controller{
    function actionAddArchiveEntry(){
        $this->deleteOldEntries();
        $citiesList = City::checkCitiesEntry();
        if($citiesList){
            if (date('H') == $this->config['cronHour'] && date('i') ==$this->config['cronMinute'] && date('s') ==$this->config['cronSecond']) {
                foreach ($citiesList as $city) {
                    $currentWeather = $this->getWeatherForCity($city->alias);
                    $archiveEntry = new Archive($currentWeather);
                    $archiveEntry->addArchiveEntry();
                }
            }
        }
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('Location:' . $host .'404');
    }
    function deleteOldEntries(){
        $archiveEntryLifeTimeInDays = $this->config['archiveEntryLifeTimeInDays'];
        $expiryDate = new \DateTime();
        $expiryDate->modify("-{$archiveEntryLifeTimeInDays} day");
        $expiryDate = $expiryDate->format('d.m.Y');
        Archive::removeOldEntires($expiryDate);
    }
}
