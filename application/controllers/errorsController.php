<?php
namespace weather\controllers;

use weather\core\Controller;

class errorsController extends Controller
{
    function action404()
    {
        http_response_code(404);
        $this->view->generate('404');
    }
    function actionNoSuchAlias(){
        $this->view->generate('no-alias');
    }

}