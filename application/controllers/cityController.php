<?php
namespace weather\controllers;

use weather\core\Controller;
use weather\core\Route;
use weather\models\Archive;
use weather\models\City;

class cityController extends Controller
{
    function actionIndex()
    {
        $citiesList = City::checkCitiesEntry();
        if($citiesList) {
            $currentCity = City::getCurrentCity();
            $cityWeather = $this->getWeatherForCity($currentCity->alias);
            $this->view->generate('list', ['citiesList' => $citiesList, 'cityWeather'=> $cityWeather, 'currentCity'=>$currentCity]);
        } else{
            City::addCityAliasList();
            $this->view->generate('add-city');
        }
    }
    function actionViewCity($currentCityAlias){
        City::setCurrentCity($currentCityAlias);
        $currentCity = City::getCurrentCity();
        $cityWeather = $this->getWeatherForCity($currentCity->alias);
        $cityForecast = $this->getForecastForCity($currentCity->alias, $this->config['period']);
        $cityArchive = Archive::getArchiveEntriesForCity($currentCity->alias);
        $this->view->generate('city',['currentCity'=>$currentCity, 'cityWeather'=> $cityWeather, 'cityForecast'=> $cityForecast, 'cityArchive' => $cityArchive]);
    }
    function actionAddCity(){
        if(isset($_REQUEST['addCity']) && $_REQUEST['addCity'] == 1){
            $data = $_POST;
            foreach ($data as &$cityProperty) {
                $cityProperty = Controller::clearSpecialChars($cityProperty);
            }
            $city = new City($data);
            $aliasExists = $city->isAliasExists();
            if($aliasExists != null) {
                $city->addCity();
                Route::ToMain();
            } else {
                Route::errorNoSuchAlias();
            }
        }
    }
    function actionEditCity(){
        if(isset($_REQUEST['editCity']) && $_REQUEST['editCity'] == 1){
            $oldAlias = $_POST['oldAlias'];
            $data['cityName'] = $_POST['editCityName'];
            $data['cityAlias'] = $_POST['editCityAlias'];
                        foreach ($data as &$cityProperty) {
                $cityProperty = Controller::clearSpecialChars($cityProperty);
            }
            $city = new City($data);
            $aliasExists = $city->isAliasExists();
            if($aliasExists != null) {
                $city->editCity($oldAlias);
                Route::ToMain();
            } else {
                Route::errorNoSuchAlias();
            }
        }
    }
    function actionDeleteCity(){
        if(isset($_REQUEST['deleteCity']) && $_REQUEST['deleteCity'] == 1){
            $cityToDelete = $_POST['deleteCityAlias'];
            $cityToDelete = Controller::clearSpecialChars($cityToDelete);
            City::deleteCity($cityToDelete);
            Archive::removeEntriesForCity($cityToDelete);
            Route::ToMain();
        }
    }
    function actionAvailable(){
        $allCities = City::getAliases();
        $this->view->generate('available', ['allCities' => $allCities]);
    }

}