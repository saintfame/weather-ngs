<?php
namespace weather\core;
class View
{
    function generate($view, $data = null)
    {
        if (is_array($data)) {
            extract($data);
        }
        $content_view = $view . '_view.php';
        include 'application/views/template_view.php';
    }
}