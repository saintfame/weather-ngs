<?php
namespace weather\core;
class Controller
{

    public $model;
    public $view;
    public $config;

    function __construct()
    {
        $this->view = new View();
        $this->config = require_once APP_ROOT . '/application/config/config.php';

    }

    protected static function clearSpecialChars($property)
    {
        $readyProperty = htmlspecialchars(trim(stripslashes($property)));
        return $readyProperty;
    }
    protected function getWeatherForCity($city)
    {
        $apiUrl = 'http://pogoda.ngs.ru/api/v1/forecasts/current?city='.$city;
        $curl = curl_init($apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curlResult = curl_exec($curl);
        curl_close($curl);
        $apiResult = json_decode($curlResult, true);
        $result['date'] = date('d.m.Y', strtotime($apiResult['forecasts'][0]['date']));
        $result['time'] = date('H:i', strtotime($apiResult['forecasts'][0]['update_date']));
        $result['temperature'] = $apiResult['forecasts'][0]['temperature'];
        $result['pressure'] = $apiResult['forecasts'][0]['pressure'];
        $result['humidity'] = $apiResult['forecasts'][0]['humidity'];
        $result['cloudness'] = $apiResult['forecasts'][0]['cloud']['title'];
        $result['precipitation'] = $apiResult['forecasts'][0]['precipitation']['title'];
        $result['alias'] = $apiResult['forecasts'][0]['links']['city'];
        return $result;
    }
    protected function getForecastForCity($city, $period)
    {
        $apiUrl = 'http://pogoda.ngs.ru/api/v1/forecasts/forecast?city='.$city;
        $curl = curl_init($apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curlResult = curl_exec($curl);
        curl_close($curl);
        $apiResult = json_decode($curlResult, true);
        if ($period > $apiResult['metadata']['resultset']['count']){
            $period = $apiResult['metadata']['resultset']['count'];
        }
        $result = null;
        for ($i = 0; $i < $period; $i++) {
            $result[$i]['date'] = date('d.m.Y', strtotime($apiResult['forecasts'][$i]['date']));
            $result[$i]['temperature'] = $apiResult['forecasts'][$i]['hours'][2]['temperature']['avg'];
            $result[$i]['precipitation'] = $apiResult['forecasts'][$i]['hours'][2]['precipitation']['title'];
            $result[$i]['cloudness'] = $apiResult['forecasts'][$i]['hours'][2]['cloud']['title'];
            $result[$i]['pressure'] = $apiResult['forecasts'][$i]['hours'][2]['pressure']['avg'];
            $result[$i]['humidity'] = $apiResult['forecasts'][$i]['hours'][2]['humidity']['avg'];
        }
        return $result;
    }
}