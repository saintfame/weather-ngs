<?php

namespace weather\core;

class Route
{
    static function start()
    {
        $controllerName = 'city';
        $actionName = 'index';
        $param = '';
        $route = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $routes = explode('/', $route);
        if (!empty($routes[1])) {
            if ($routes[1] == '404') {
                $controllerName = 'errors';
                $actionName = '404';
            } elseif($routes[1] == 'cronJobURLThatCanNotBeEnteredAccidently'){
                $controllerName = 'archive';
                $actionName = 'addArchiveEntry';
            } elseif($routes[1] == 'archive'){
                Route::ErrorPage404();
            } elseif($routes[1] == 'error'){
                $controllerName = 'errors';
                $actionName = 'noSuchAlias';
            } else {
                $controllerName = $routes[1];
            }
        }
        if (!empty($routes[2])) {
            $actionName = $routes[2];
        }
        if (!empty($routes[3])) {
            $param = $routes[3];
        }
        $controller = 'weather\\controllers\\' . $controllerName . 'Controller';
        $controllerFile = 'application/controllers/' . $controllerName . 'Controller' . '.php';
        if (file_exists($controllerFile)) {
            $test = new $controller();
            $action = 'action' . ucfirst($actionName);
            if (method_exists($controller, $action)) {
                if (isset($param) && !empty($param)) {
                        $test->{$action}($param);
                } else {
                    $test->{$action}();
                }
            } else {
                Route::ErrorPage404();
            }
        } else {
            Route::ErrorPage404();
        }
    }

    static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('Location:' . $host . '404');
        die;
    }
    static function ToMain(){
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('Location:' . $host);
    }
    static function errorNoSuchAlias(){
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('Location:' . $host. 'error');
    }

}