<div id="deleteCity" class="modal">
    <div class="modal-content">
        <span class="close">×</span>
        <h2 class="modal-label">Удалить город</h2>
        <span class="confirm-delete">Вы уверены, что хотите удалить ?</span>
        <form id="deleteCityForm" action="/city/deleteCity" method="post">
        <input type="hidden" name="deleteCity" value="1"/>
        <input type="hidden" name="deleteCityAlias" id="deleteCityAlias"value=""/>
        <div class="right-buttons">
            <button id="deleteCityOK" class="button right">Удалить</button>
            <button class="button right cancel">Отменить</button>
        </div>
    </div>
</div>