<div class="container">
    <div class="current-city-block">
        <?php include 'currentCityBlock.php';?>
    </div>
    <div class="city-list-block">
        <button id="addСityButton" class="button">Добавить город</button>
        <?php if($citiesList): ?>
        <div class="city-list-table">
            <table width="80%" border="1" cellpadding="4" cellspacing="0" id="cityList">
                <thead>
                <tr>
                    <th colspan="3">Избранные города</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($citiesList as $city): ?>
                        <tr>
                            <td width="50%" data-id="pickCity" data-alias="<?php echo $city->alias;?>" class="link">
                                <?php echo $city->name;?>
                            </td>
                            <td data-id="changeCity" data-alias="<?php echo $city->alias;?>" data-name="<?php echo $city->name;?>" class="link in-table-button">
                                Изменить
                            </td>
                            <td data-id="deleteCity" data-alias="<?php echo $city->alias;?>" data-name="<?php echo $city->name;?>" class="link in-table-button">
                                Удалить
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <?php endif;?>
        </div>
    </div>
</div>
<?php include 'addCityModal.php';?>
<?php include 'editCityModal.php';?>
<?php include 'deleteCityModal.php';?>
