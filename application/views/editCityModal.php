<div id="editCity" class="modal">
    <div class="modal-content">
        <span class="close">×</span>
        <h2 class="modal-label">Изменить город</h2>
        <form id="editCityForm" action="/city/editCity" method="post">
            <input type="hidden" name="editCity" value="1">
            <input type="hidden" name="oldAlias" value="">
            <input type="text" name="editCityName" id="cityName" placeholder="Город" value="" required/>
            <input type="text" name="editCityAlias" id="cityAlias" placeholder="Alias" value="" required/>
            <div class="right-buttons">
                <button id="editCityOK" class="button right">Сохранить</button>
                <button class="button right cancel">Отменить</button>
            </div>
        </form>
    </div>
</div>