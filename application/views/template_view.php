<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link href="/assets/css/style.css" rel="stylesheet" type="text/css" >
    </head>
    <body>
        <?php use weather\core\View; ?>
        <?php include 'application/views/'.$content_view ?>
        <script defer src="/assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
        <script defer src="/assets/js/app.js" type="text/javascript"></script>
    </body>
</html>
