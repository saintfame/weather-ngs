<?php if($cityWeather && $currentCity): ?>
    <table border="1" cellpadding="4" cellspacing="0" width="80%">
        <tr>
            <td>
                Выбран город: <?php echo $currentCity->name;?>. <br>
                Сейчас в городе <?php echo $cityWeather['cloudness'];?>, <?php echo $cityWeather['precipitation'];?>.
                Температура <?php echo $cityWeather['temperature'];?> &deg;С,
                Атм.давление <?php echo $cityWeather['pressure'];?> мм,
                влажность <?php echo $cityWeather['humidity'];?>%.
            </td>
        </tr>
    </table>
<?php endif;?>