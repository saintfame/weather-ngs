<div class="container">
    <?php include 'buttonToMain.php';?>
    <div class="city-list-block">
        <?php if($allCities): ?>
        <div class="city-list-table">
            <table width="80%" border="1" cellpadding="4" cellspacing="0" id="cityList">
                <thead>
                <tr>
                    <th colspan="2">Доступные города</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($allCities as $city): ?>
                    <tr>
                        <td width="50%">
                            <?php echo $city->name;?>
                        </td>
                        <td width="50%">
                            <?php echo $city->alias;?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <?php endif;?>
        </div>
    </div>
    <?php include 'buttonToMain.php';?>

</div>