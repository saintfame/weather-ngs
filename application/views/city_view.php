<div class="container">
    <div class="current-city-block">
        <?php if ($cityWeather && $currentCity): ?>
            <h2><?php echo $currentCity->name; ?></h2>
            <?php include 'currentCityBlock.php'; ?>
        <?php endif; ?>
    </div>
    <div class="city-forecast-block">
        <table width="80%" border="1" cellpadding="4" cellspacing="0" id="cityForecast">
            <thead>
            <tr>
                <th colspan="6">Прогноз</th>
            </tr>
            <tr>
                <th width="10%">
                    Дата
                </th>
                <th class="forecast-archive-table-content">
                    Дневная температура
                </th>
                <th class="forecast-archive-table-content">
                    Облачность
                </th>
                <th class="forecast-archive-table-content">
                    Осадки
                </th>
                <th class="forecast-archive-table-content">
                    Влажность
                </th>
                <th class="forecast-archive-table-content">
                    Атм.давление
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if ($cityForecast): ?>
                <?php foreach ($cityForecast as $day): ?>
                    <tr class="forecast-archive-table-content">
                        <td>
                            <?php echo($day['date']); ?>
                        </td>
                        <td>
                            <?php echo($day['temperature']); ?> &deg;С
                        </td>
                        <td>
                            <?php echo($day['cloudness']); ?>
                        </td>
                        <td>
                            <?php echo($day['precipitation']); ?>
                        </td>
                        <td>
                            <?php echo $day['humidity']; ?>%
                        </td>
                        <td>
                            <?php echo $day['pressure']; ?> мм
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="city-history-block">
        <table width="80%" border="1" cellpadding="4" cellspacing="0" id="cityArchive">
            <thead>
            <tr>
                <th colspan="6">Архив</th>
            </tr>
            <tr>
                <th width="10%">
                    Дата
                </th>
                <th class="forecast-archive-table-content">
                    Температура
                </th>
                <th class="forecast-archive-table-content">
                    Облачность
                </th>
                <th class="forecast-archive-table-content">
                    Осадки
                </th>
                <th class="forecast-archive-table-content">
                    Влажность
                </th>
                <th class="forecast-archive-table-content">
                    Атм.давление
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if ($cityArchive): ?>
                <?php foreach ($cityArchive as $day): ?>
                    <tr class="forecast-archive-table-content">
                        <td>
                            <?php echo($day->date); ?>
                        </td>
                        <td>
                            <?php echo($day->temperature); ?> &deg;С
                        </td>
                        <td>
                            <?php echo($day->cloudness); ?>
                        </td>
                        <td>
                            <?php echo($day->precipitation); ?>
                        </td>
                        <td>
                            <?php echo($day->humidity); ?>%
                        </td>
                        <td>
                            <?php echo($day->pressure); ?> мм
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?php include 'buttonToMain.php'; ?>
</div>
