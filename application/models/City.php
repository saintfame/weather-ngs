<?php

namespace weather\models;

use MongoDB\Client;
use weather\core\Model;

//use weather\core\db;
class City extends Model
{
    public $name;
    public $alias;

    public function __construct($data = array())
    {
        parent::__construct();
        if (isset($data['cityName']) && isset($data['cityAlias'])) {
            $this->name = $data['cityName'];
            $this->alias = $data['cityAlias'];
        }
    }

    public static function checkCitiesEntry()
    {
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->cities;
        $cursor = $collection->find();
        $i = 0;
        $results = [];
        foreach ($cursor as $document) {
            $i++;
            $array = ['cityName' => $document->name, 'cityAlias' => $document->alias];
            $city = new City($array);
            array_push($results, $city);
        }
        if ($i > 0) {
            return $results;
        } else {
            return false;
        }
    }

    public static function getCurrentCity()
    {
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->lastCity;
        $document = $collection->findOne();
        $collection = $client->weather->cities;
        if ($document != null) {
            $document2 = $collection->findOne(['alias' => $document->alias]);
        } else {
            $document2 = $collection->findOne();
            $collection = $client->weather->lastCity;
            $insertOneResult = $collection->insertOne(['alias' => $document2->alias]);
        }
        $array = ['cityName' => $document2->name, 'cityAlias' => $document2->alias];
        $city = new City($array);
        return $city;
    }

    public function addCity()
    {
        $collection = $this->client->weather->cities;
        $insertOneResult = $collection->insertOne(['name' => $this->name, 'alias' => $this->alias]);
        $collection = $this->client->weather->lastCity;
        $document = $collection->findOne();
        if ($document = null) {
            $insertOneResult = $collection->insertOne(['alias' => $this->alias]);
        }
    }

    public function editCity($oldAlias)
    {
        $collection = $this->client->weather->cities;
        $updateResult = $collection->updateOne(
            ['alias' => $oldAlias],
            ['$set' => ['name' => $this->name, 'alias' => $this->alias]]
        );
        $collection = $this->client->weather->lastCity;
        $updateResult = $collection->updateOne(
            ['alias' => $oldAlias],
            ['$set' => ['alias' => $this->alias]]
        );
    }

    public function isAliasExists()
    {
        $collection = $this->client->weather->cityAliases;
        $document = $collection->findOne(['alias' => $this->alias]);
        return $document;
    }
    public static function getAliases(){
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->cityAliases;
        $cursor = $collection->find();
        $i = 0;
        $results = [];
        foreach ($cursor as $document) {
            $i++;
            $array = ['cityName' => $document->name, 'cityAlias' => $document->alias];
            $city = new City($array);
            array_push($results, $city);
        }
        if ($i > 0) {
            return $results;
        } else {
            return false;
        }
    }

    public static function setCurrentCity($cityAlias)
    {
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->lastCity;
        $updateResult = $collection->updateOne(
            [],
            ['$set' => ['alias' => $cityAlias]]
        );
    }

    public static function deleteCity($cityToDelete)
    {
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->cities;
        $deleteResult = $collection->deleteOne(['alias' => $cityToDelete]);
        $collection = $client->weather->lastCity;
        $document = $collection->findOne(['alias' => $cityToDelete]);
        if ($document != null) {
            $deleteResult = $collection->deleteOne(['alias' => $cityToDelete]);
        }
    }

    public static function addCityAliasList()
    {
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->cityAliases;
        $collection->drop();
        $apiUrl = 'http://pogoda.ngs.ru/api/v1/cities';
        $curl = curl_init($apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curlResult = curl_exec($curl);
        curl_close($curl);
        $allCities = json_decode($curlResult, true);
        $countCities = $allCities['metadata']['resultset']['count'];
        for ($i = 0; $i < $countCities; $i++) {
            $insertOneResult = $collection->insertOne(['alias' => $allCities['cities'][$i]['alias'],'name'=>$allCities['cities'][$i]['title']]);
        }
    }
}