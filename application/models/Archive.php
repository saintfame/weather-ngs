<?php

namespace weather\models;

use MongoDB\Client;
use weather\core\Model;
class Archive extends Model
{
    public $date;
    public $time;
    public $alias;
    public $temperature;
    public $pressure;
    public $humidity;
    public $cloudness;
    public $precipitation;


    public function __construct($data = array())
    {
        parent::__construct();
        if (
            isset($data['date']) &&
            isset($data['time']) &&
            isset($data['alias']) &&
            isset($data['temperature']) &&
            isset($data['pressure']) &&
            isset($data['humidity']) &&
            isset($data['cloudness']) &&
            isset($data['precipitation'])) {
            $this->date = $data['date'];
            $this->time = $data['time'];
            $this->alias = $data['alias'];
            $this->temperature = $data['temperature'];
            $this->pressure = $data['pressure'];
            $this->humidity = $data['humidity'];
            $this->cloudness = $data['cloudness'];
            $this->precipitation = $data['precipitation'];
        }
    }
    public function addArchiveEntry(){
        $collection = $this->client->weather->archive;
        $insertOneResult = $collection->insertOne([
                'alias' => $this->alias,
                'date' =>$this->date,
                'time'=> $this->time,
            'temperature' => $this->temperature,
                'pressure'=>$this->pressure,
            'humidity' => $this->humidity,
            'cloudness' => $this->cloudness,
            'precipitation' => $this->precipitation
            ]);
    }
    public static function removeOldEntires($expiryDate){
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->archive;
        $deleteResult = $collection->deleteMany(['date' => $expiryDate]);
    }
    public static function removeEntriesForCity($city){
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->archive;
        $deleteResult = $collection->deleteMany(['alias' => $city]);
    }
    public static function getArchiveEntriesForCity($cityAlias){
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->weather->archive;
        $cursor = $collection->find(['alias'=>$cityAlias]);
        $i = 0;
        $results = [];
        foreach ($cursor as $document) {
            $i++;
            $array = (array)$document;
            unset($array['_id']);
            $archiveEntry = new Archive($array);
            array_push($results, $archiveEntry);
        }
        if ($i > 0) {
            return $results;
        } else {
            return false;
        }
    }
}