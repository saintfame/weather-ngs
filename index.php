<?php
ini_set('display_errors', 1);
define('APP_ROOT', realpath(__DIR__) . '/');
require_once 'application/init.php';
$router = new \weather\core\Route();
$router->start();
